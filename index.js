//creo servidor
const express = require('express'); //importo lo que me da express, hay un default definido para compartirme 
const bodyparser = require('body-parser');

const app = express(); //app es aplicacion servidor
app.use(bodyparser.json()); //le digo a la app que use json el use hace que se ejecute antes del resto

app.get('/', function(request,response){ //creo una ruta, la funcion que se ejecuta recibe request y response
    //procesamiento de un request GET a /
    console.log(request.query.a); //devuelve por consola, no por bash
    console.log(request.query.b);
    response.send('Hola Express\n');
});

app.get('/', function(request,response){ //misma ruta y parametro ya registrado, no lo ejecuta solo el primero
    response.send('sale esto? repetido')
});    

app.post('/', function (request,response){
    //console.log(request.query.a); //devuelve por consola, no por bash lo que recibe en la url curl localhost:5000&a=1 -X POST
    console.log(request.body);
    response.send('Desde POST '+request.body.param1+'\n');
});

app.listen(port=5000,function(){ //empieza a escuchar, carga todo y cuando termina ejecuta la funcion que le paso
    console.log("Servidor Escuchando...");
});

console.log("Hola Mundo NodeJs");