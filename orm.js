const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');


const app = express();

app.use(bodyParser.json()); 

//Modelo, defino un objeto, le digo la base usuario null y contraseña null y las especificaciones de la base que quiero
const sequelize = new Sequelize('mainDB',null,null,{
    dialect: "sqlite",
    storage: "./mainDB.sqlite"
});

sequelize.authenticate() //devuelve una promesa cuando se autentica correctamente ejecuta el then
    .then(function(){ console.log('Autenticado!') })
    .catch(function(){ console.log('No me autentica') });

var User = sequelize.define('Departamento', {
    nombre: Sequelize.STRING
});

var Persona = sequelize.define('Persona', {
    nombre: Sequelize.STRING,
    apellido: Sequelize.STRING
});

sequelize.sync()
    .then(function(err){
        console.log('Sincronizado');
    })
    .catch(function(err){
        console.log('Error al sincronizar '+err);
    });